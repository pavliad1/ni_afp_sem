module Styles exposing
    ( background
    , square
    , gameover
    , menuitem
    , selectedStyle
    , pressenter
    )

import Html exposing (Attribute)
import Html.Attributes as Attributes
import Html.Attributes exposing (selected)
import Menu exposing (MenuItem)
import Menu exposing (isSameItem)

background : Int -> Int -> List (Attribute msg)
background width height =
    [ Attributes.style "margin" "auto"
    , Attributes.style "position" "relative"
    , Attributes.style "top" "50px"
    , Attributes.style "height" (String.append (String.fromInt height) "px")
    , Attributes.style "width" (String.append (String.fromInt width) "px")
    , Attributes.style "background-color" "rgb(185, 180, 180)"
    ]

square : Int -> String -> List (Attribute msg)
square size color =
    [ Attributes.style "height" (String.append (String.fromInt size) "px")
    , Attributes.style "width" (String.append (String.fromInt size) "px")
    , Attributes.style "position" "absolute"
    , Attributes.style "background-color" color
    , Attributes.style "border" "1px solid black"
    ]

gameover : List (Attribute msg)
gameover =
    [ Attributes.style "margin" "auto"
    , Attributes.style "height" "180px"
    , Attributes.style "position" "absolute"
    , Attributes.style "text-transform" "uppercase"
    , Attributes.style "text-align" "center"
    , Attributes.style "font-size" "70px"
    , Attributes.style "background-color" "rgb(185, 180, 180)"
    , Attributes.style "border" "2px solid black"
    ]

pressenter : List (Attribute msg)
pressenter =
    [ Attributes.style "margin" "auto"
    , Attributes.style "position" "relative"
    , Attributes.style "top" "180px"
    , Attributes.style "text-transform" "uppercase"
    , Attributes.style "text-align" "center"
    , Attributes.style "font-size" "30px"
    , Attributes.style "background-color" "rgb(185, 180, 180)"
    , Attributes.style "border" "1px solid black"
    ]

menuitem : List (Attribute msg)
menuitem =
    [ Attributes.style "margin" "auto"
    , Attributes.style "position" "relative"
    , Attributes.style "text-transform" "uppercase"
    , Attributes.style "text-align" "center"
    , Attributes.style "font-size" "30px"
    --, Attributes.style "border" "1px solid black"
    ]

selectedStyle : MenuItem -> MenuItem -> List (Attribute msg)
selectedStyle item selected = 
    if isSameItem item selected then [ Attributes.style "color" "rgb(255, 0, 0)" ] else []
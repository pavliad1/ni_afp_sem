module Player exposing(..)

import List.Extra as Extra
import Color exposing (Color)

type Brick
    = Empty
    | Filled Color

type alias Board = 
    { grid: List (List Brick)
    , width: Int
    , height: Int
    }

type alias Player = 
    { x : Int
    , y : Int
    , board : Board
    }

isFilled : Brick -> Bool
isFilled b = 
    case b of
        Empty -> False
        _     -> True

getColor : Brick -> Maybe Color
getColor b =
    case b of
        (Filled c) -> Just c
        _          -> Nothing

getCSSColor : Brick -> String
getCSSColor brick = 
    case brick of
        (Filled c) -> Color.toCssString c
        _          -> ""


yellowBrick : Brick
yellowBrick = Filled Color.yellow

lightBlueBrick : Brick
lightBlueBrick = Filled Color.lightBlue

redBrick : Brick
redBrick = Filled Color.red

greenBrick : Brick
greenBrick = Filled Color.green
    
lightOrangeBrick : Brick
lightOrangeBrick = Filled Color.lightOrange

pinkBrick : Brick
pinkBrick = Filled (Color.rgb255 255 20 147) 

purpleBrick : Brick
purpleBrick = Filled Color.purple

createM : Int -> Int -> Player
createM x y =
    { x = x
    , y = y
    , board = { width = 2
              , height = 2
              , grid = [ [ yellowBrick, yellowBrick ]
                       , [ yellowBrick, yellowBrick ]
                       ]
              }
    }

createI : Int -> Int -> Player
createI x y =
    { x = x
    , y = y
    , board = { width = 1
              , height = 4
              , grid = [ [ lightBlueBrick ]
                       , [ lightBlueBrick ]
                       , [ lightBlueBrick ]
                       , [ lightBlueBrick ]
                       ]
              }
    }

createS : Int -> Int -> Player
createS x y =
    { x = x
    , y = y
    , board = { width = 2
              , height = 3
              , grid = [ [ redBrick, Empty    ]
                       , [ redBrick, redBrick ]
                       , [ Empty,    redBrick ]
                       ]
              }
    }

createZ : Int -> Int -> Player
createZ x y =
    { x = x
    , y = y
    , board = { width = 2
              , height = 3
              , grid = [ [ Empty,      greenBrick ]
                       , [ greenBrick, greenBrick ]
                       , [ greenBrick, Empty      ]
                       ]
              }
    }

createL : Int -> Int -> Player
createL x y =
    { x = x
    , y = y
    , board = { width = 2
              , height = 3
              , grid = [ [ lightOrangeBrick, Empty ]
                       , [ lightOrangeBrick, Empty ]
                       , [ lightOrangeBrick, lightOrangeBrick]
                       ]
              }
    }

createJ : Int -> Int -> Player
createJ x y =
    { x = x
    , y = y
    , board = { width = 2
              , height = 3
              , grid = [ [ Empty,     pinkBrick ]
                       , [ Empty,     pinkBrick ]
                       , [ pinkBrick, pinkBrick ]
                       ]
              }
    }

createT : Int -> Int -> Player
createT x y =
    { x = x
    , y = y
    , board = { width = 3
              , height = 2
              , grid = [ [ Empty,       purpleBrick, Empty     ]
                       , [ purpleBrick, purpleBrick, purpleBrick ]
                       ]
              }
    }

rotate : Player -> Player
rotate {x, y, board} =
    let rotatedGrid = board.grid |> List.reverse |> Extra.transpose
    in { x = x, y = y, board = { width = board.height, height = board.width, grid = rotatedGrid } }


left : Player -> Player
left player = { player | x = player.x - 1 }

right : Player -> Player
right player = { player | x = player.x + 1 }

down : Player -> Player
down player = { player | y = player.y + 1 }

up : Player -> Player
up player = { player | y = player.y - 1 }

{-

type alias Field = 
    {
        x : Int,
        y : Int,
        brick: Int
    }

blockMatrixFromLists : List (List Brick) -> Matrix.Matrix Brick
blockMatrixFromLists list = withDefault Matrix.empty (Matrix.fromLists list)

rotateBlock : Player -> Player
rotateBlock block = { block | blockMatrix = (Matrix.toLists (.blockMatrix block)) |> List.reverse |> blockMatrixFromLists |> Matrix.transpose }

height : Player -> Int
height { blockMatrix } = Matrix.height blockMatrix

width : Player -> Int
width { blockMatrix } = Matrix.width blockMatrix

createEl : Int -> Int -> Player
createEl x y =
    { x = x
    , y = y
    , blockMatrix = blockMatrixFromLists [ [ Filled, Empty ]
                                         , [ Filled, Empty ]
                                         , [ Filled, Filled]
                                         ]
    }

lu = [ [1,2], [3,4] ]

--( List.indexedMap (\ii vv -> {x = i, y = ii, z = vv} ) v )

--test = List.indexedMap (\i v -> (i, v)) lu
blockToFieldList = List.indexedMap (\i v -> (List.indexedMap (\ii vv -> (Field i ii vv) ) v)) lu |> List.concat

fieldComparator : Comparator Field
fieldComparator = Compare.concat [ (Compare.by .x), (Compare.by .y) ]

fieldListGetRange : Int -> Int -> Int -> Int -> List Field -> List Field
fieldListGetRange ax ay aw ah list =
    List.filter (\{x,y} -> (x >= ax && (x < ax + aw) && y >= ay && (y < ay + ah))) list
-}
module Game exposing (..)

import Player exposing(..)

type Direction
    = Left
    | Right
    | Down
    | Up
    | Invalid

type Score
    = Zero
    | Score Int

fastDrop : Player -> Board -> Player
fastDrop p b =
    let np = down p
    in
    if isCollision np b then
            p
        else
            fastDrop np b

movePlayer : Player -> Board -> Direction -> ( Player, Bool )
movePlayer player board dir =
    let newPos = case dir of
                    Left -> left player
                    Right -> right player
                    Down -> down player
                    Up   -> rotate player
                    Invalid -> player
    in if isCollision newPos board then (player, False) else (newPos, True)

clearGameBoard : Int -> Int -> Board
clearGameBoard w h = {width = w, height = h, grid = List.repeat h (List.repeat w Empty) }

clearFullRows : Board -> (Board, Score)
clearFullRows board =
    case clearFullRowsRec board.grid [] 0 of
       (_, 0)  -> (board, Zero)
       (grid, a) -> ( { board | grid = (List.repeat a (List.repeat board.width Empty)) ++ grid }, Score a )

clearFullRowsRec : List (List Brick) -> List (List Brick) -> Int -> (List (List Brick), Int)
clearFullRowsRec list acc count =
    case list of
        (x::xs) ->  if ((List.filter (\e -> e == Empty) x) |> List.length) == 0 then
                        clearFullRowsRec xs acc (count + 1)
                    else
                        clearFullRowsRec xs (acc ++ [x]) count
        _       -> (acc, count)

-- check if out of bounds here !
isCollision : Player -> Board -> Bool
isCollision player board =
    let maxX = (.x player) + (.board player |> .width)
        maxY = (.y player) + (.board player |> .height)
        boardWidth = (.width board)
        boardHeight = (.height board)
    in
        if (maxX > boardWidth || maxY > boardHeight || player.x < 0 || player.y < 0) then
            True
        else
            isCollisionRec (.board player |> .grid) (.grid board) (.x player) (.y player) 0

isCollisionRec : List (List Brick) -> List (List Brick) -> Int -> Int -> Int -> Bool
isCollisionRec a b posX posY row = 
    case (a, b) of
        (x::xs, y::ys) -> if row < posY then
                                isCollisionRec a ys posX posY (row + 1)
                          else
                                if (isCollisionRow x (List.drop posX y)) then
                                    True
                                else
                                    isCollisionRec xs ys posX posY (row + 1)
        _ -> False

isCollisionRow : List Brick -> List Brick -> Bool
isCollisionRow what to =
    case (what, to) of
        (x::xs, y::ys) -> case (x, y) of
                                (Filled _, Filled _) -> True
                                _                    -> isCollisionRow xs ys
        _ -> False
        

-- a = what, b = to
imprint : Player -> Board -> Board
imprint player board = {board | grid = imprintRec player.board.grid board.grid player.x player.y 0 }

-- a = what, b = to
imprintRec : List (List Brick) -> List (List Brick) -> Int -> Int -> Int -> List (List Brick)
imprintRec a b posX posY row = 
    case (a, b) of
        (x::xs, y::ys) -> if row < posY then
                                [y] ++ (imprintRec a ys posX posY (row + 1))
                          else
                                [(imprintRow x (List.drop posX y) (List.take posX y))] ++ (imprintRec xs ys posX posY (row + 1))
        _ -> b

imprintRow : List Brick -> List Brick -> List Brick -> List Brick
imprintRow what to acc =
    case (what, to) of
        (x::xs, y::ys) -> case (x, y) of
                                (Empty, Empty) -> imprintRow xs ys (acc ++ [Empty])
                                (Empty, Filled a) -> imprintRow xs ys (acc ++ [Filled a])
                                (Filled a, Empty) -> imprintRow xs ys (acc ++ [Filled a])
                                (Filled a, Filled b) -> imprintRow xs ys (acc ++ [Filled a]) -- this one is collision but it wont happen since its tested for collisions before anyway
        _ -> acc ++ to
module Main exposing (main)

import Browser
import Html exposing (Html, div, text)
import Browser.Events exposing (onKeyPress)
import Json.Decode as Decode
import Html.Attributes as Attributes
import Styles
import Player exposing(..)
import Game exposing (..)
import Random exposing (Seed, initialSeed)
import Maybe
import Time
import Task
import Menu as Menu
import Menu exposing (createMenu)
import Menu exposing (Menu)
import Menu exposing (getDifficulty)

main =
  Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

brickSize = 30
boardWidth = 10
boardHeight = 20

type alias Model =
    { menu: Menu.Menu
    , selectedItem: Menu.MenuItem
    , isRunning: Bool
    , player: Player
    , board: Board
    , score: Int
    , nextSeed: Seed
    , window: Window
    }

type Msg
    = CharacterKey Char
    | ControlKey String
    | OnTime Time.Posix
    | Tick Time.Posix

type DifficultyChanger
    = Easier
    | Harder

type Window
    = Menu
    | Game

getTime : Cmd Msg
getTime = 
    Task.perform OnTime Time.now

createRandomPlayer : Int -> Int -> Seed -> Player
createRandomPlayer x y seed =
    let list = [createM, createI, createS, createZ, createL, createJ, createT]
        (val, _) = Random.step (Random.int 0 (List.length list)) seed
    in (Maybe.withDefault createM (List.drop val list |> List.head)) x y

hitBlock model =
    let imprintedBoard = imprint model.player model.board
        p = createRandomPlayer ((round (boardWidth / 2)) - 1) 0 model.nextSeed
    in
        if isCollision p imprintedBoard then
            ( { model | isRunning = False }, Cmd.none )
        else
            case clearFullRows imprintedBoard of
                (_, Zero) -> ({ model | player = p, board = imprintedBoard }, getTime )
                (scoredBoard, Score a) -> ( { model | player = p, board = scoredBoard, score = model.score + a }, getTime )

restartGame : Model -> Model
restartGame model = 
    { model
    | window = Menu
    , board = clearGameBoard boardWidth boardHeight
    , player = createRandomPlayer ((round (boardWidth / 2)) - 1) 0 model.nextSeed
    , isRunning = False
    , score = 0
    , selectedItem = Menu.Start
    }

init : () -> ( Model, Cmd Msg )
init _ =
    ( { menu = createMenu False
      , selectedItem = Menu.Start
      , isRunning = False 
      , player = createRandomPlayer ((round (boardWidth / 2)) - 1) 0 (initialSeed 0)
      , board = clearGameBoard boardWidth boardHeight
      , score = 0
      , nextSeed = initialSeed 0
      , window = Menu
      } , Cmd.none )


changeDifficulty : Model -> DifficultyChanger -> ( Model, Cmd Msg )
changeDifficulty model diff = 
    if not model.isRunning then
        case model.selectedItem of
           Menu.Difficulty _ ->
                case diff of 
                    Easier -> ( { model | menu = Menu.decreaseDifficulty model.menu }, Cmd.none )
                    Harder -> ( { model | menu = Menu.increaseDifficulty model.menu }, Cmd.none )
           _ -> ( model, Cmd.none )
    else
        ( model, Cmd.none )

onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.selectedItem of
       Menu.Start -> if not model.isRunning then
                    ( { model | window = Game, isRunning = True } ,Cmd.none )
                else
                    ( { model | window = Game } ,Cmd.none )
       Menu.Restart -> ( restartGame model,Cmd.none )
       _ -> ( model, Cmd.none )

pauseGame : Model -> ( Model, Cmd Msg )
pauseGame model =
    ( { model | window = Menu }, Cmd.none )

updateMenu : Msg -> Model -> ( Model, Cmd Msg )
updateMenu msg model =
    case msg of
       CharacterKey 'a' -> changeDifficulty model Easier
       CharacterKey 'd' -> changeDifficulty model Harder
       CharacterKey 'w' -> ( { model | selectedItem = Menu.scrollUp model.menu model.selectedItem model.isRunning }, Cmd.none )
       CharacterKey 's' -> ( { model | selectedItem = Menu.scrollDown model.menu model.selectedItem model.isRunning }, Cmd.none )
       ControlKey "Enter" -> onEnter model
       _ -> ( model, Cmd.none )

updateGame : Msg -> Model -> ( Model, Cmd Msg )
updateGame msg model = 
    if not model.isRunning then
        case msg of
            ControlKey "Enter" -> if not model.isRunning then ( restartGame model ,Cmd.none ) else ( model, Cmd.none )
            _ -> ( model, Cmd.none )
    else
    case msg of
        OnTime time -> ( { model | nextSeed = Time.posixToMillis time |> initialSeed }, Cmd.none )
        CharacterKey ' ' -> hitBlock { model | player = fastDrop model.player model.board }
        CharacterKey 'p' -> pauseGame model
        CharacterKey k -> let dir = case k of
                                        'a' -> Left
                                        'd' -> Right
                                        'w' -> Up
                                        's' -> Down
                                        _   -> Invalid
                                    in case movePlayer model.player model.board dir of
                                        (newPos, True) ->
                                            ( { model | player = newPos }, Cmd.none )
                                        (_, False) ->
                                            if dir == Down then
                                                hitBlock model
                                            else
                                                ( model, Cmd.none )
        Tick newTime ->
            case movePlayer model.player model.board Down of
                (newPos, True) -> ( { model | player = newPos }, Cmd.none )
                (_, False)    -> hitBlock model
        _ ->
            ( model, Cmd.none )

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model.window of
       Menu -> updateMenu msg model
       Game -> updateGame msg model

type alias Field = 
    {
        x : Int,
        y : Int,
        brick: Brick
    }

blockToFieldList : Board -> List Field
blockToFieldList {grid} = List.indexedMap (\i v -> (List.indexedMap (\ii vv -> (Field ii i vv) ) v)) grid |> List.concat

px val = String.append (String.fromInt (val * brickSize)) "px"

fn : Field -> Html Msg
fn {x,y, brick} =
    --div (Styles.square ++ [Attributes.style "left" (px (x))] ++ [Attributes.style "top" (px (y))]) []
    div (Styles.square brickSize (getCSSColor brick) ++ [Attributes.style "left" (px x), Attributes.style "top" (px y)]) []
       

createDivs : Board -> List (Html.Html Msg)
createDivs board = 
    let list = List.filter (\{brick} -> isFilled brick ) (blockToFieldList board)
    in List.map fn list

createPlayerDivs : Player -> List (Html.Html Msg)
createPlayerDivs player =
    let list = List.filter (\{brick} ->  isFilled brick ) (blockToFieldList player.board)
    in List.map fn (List.map (\{x,y,brick} -> {brick = brick, x = x + player.x, y = y + player.y}) list)

createScore : Int -> List (Html.Html Msg)
createScore score = 
    [Html.h1 [] [text (String.fromInt score)]]

createGameOver : Bool -> List (Html.Html Msg)
createGameOver isRunning = 
    if not isRunning then
        [Html.h1 Styles.gameover [text "game over"], Html.h3 Styles.pressenter [text "press enter"]]
    else
        []

viewGame : Model -> Html Msg
viewGame model =
  div (Styles.background (model.board.width * brickSize) (model.board.height * brickSize) )
    ((createDivs model.board) ++ (createPlayerDivs model.player) ++ (createScore model.score) ++ (createGameOver model.isRunning))

viewMenuItems : Model -> Html Msg
viewMenuItems model =
    let drawableList = List.filter (\e -> if model.isRunning then True else e /= Menu.Restart) model.menu
    in
    Html.div [] (List.map (\e -> Html.div (Styles.menuitem ++ (Styles.selectedStyle e model.selectedItem)) [ text (Menu.menuItemToString e model.isRunning) ]) drawableList)

viewMenu : Model -> Html Msg
viewMenu model =
  div (Styles.background (boardWidth * brickSize) (boardHeight * brickSize) )
    [ viewMenuItems model ]

view : Model -> Html Msg
view model = 
    case model.window of
       Menu -> viewMenu model
       Game -> viewGame model

keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toKey (Decode.field "key" Decode.string)


toKey : String -> Msg
toKey keyValue =
    case String.uncons keyValue of
        Just ( char, "" ) ->
            CharacterKey char
        _ ->
            ControlKey keyValue

subscriptions : Model -> Sub Msg
subscriptions model =
    let diff = Maybe.withDefault (Menu.Static 1) (getDifficulty model.menu)
        val = case diff of
                    Menu.Dynamic -> (toFloat (if model.score < 10 then model.score else 10))
                    Menu.Static a -> (toFloat a)
    in Sub.batch [ onKeyPress keyDecoder, Time.every (500 - val * 40) Tick ]
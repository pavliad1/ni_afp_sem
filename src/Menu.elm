module Menu exposing (..)

import Html exposing (Html, button, div, text)
import Maybe exposing (withDefault)
import Html.Attributes exposing (selected)
import Html exposing (main_)

type Difficulty
    = Dynamic
    | Static Int

type MenuItem
    = Start
    | Restart
    | Difficulty Difficulty

type alias Menu = List MenuItem

createMenu : Bool -> Menu
createMenu _ = [Start, Restart, Difficulty (Static 1)]

getDifficulty : Menu -> Maybe Difficulty
getDifficulty menu = 
    case menu of 
        (x::xs) -> case x of
                        Difficulty d -> Just d
                        _ -> getDifficulty xs
        _ -> Nothing

increaseDifficulty : Menu -> Menu
increaseDifficulty menu = 
    List.map (\e ->
                case e of
                    Difficulty dif -> case dif of
                                        Static a -> if a == 10 then Difficulty Dynamic else Difficulty (Static (a + 1))
                                        _ -> Difficulty Dynamic
                    _ -> e
             )
             menu

decreaseDifficulty : Menu -> Menu
decreaseDifficulty menu = 
    List.map (\e ->
                case e of
                    Difficulty dif -> case dif of
                                        Static a -> if a > 1 then Difficulty (Static (a - 1)) else Difficulty (Static a)
                                        _ -> Difficulty (Static 10)
                    _ -> e
             )
             menu

menuItemToString : MenuItem -> Bool -> String
menuItemToString item isRunning = 
    case item of
        Start -> if isRunning then "Continue" else "Start"
        Restart -> "Restart"
        Difficulty dif -> String.append "Difficulty: " (case dif of
                                                            Dynamic -> "Dynamic"
                                                            Static a -> (String.fromInt a))


scrollDown : Menu -> MenuItem -> Bool -> MenuItem
scrollDown menu selectedItem isRunning =
    case menu of
        [] -> selectedItem 
        (x::xs) ->  if isSameItem x selectedItem then
                        let l = List.head xs
                        in case l of
                            Nothing -> selectedItem
                            Just i  ->  if (not isRunning) && (i == Restart) then
                                            withDefault selectedItem (List.head (List.drop 1 xs))
                                        else
                                            i
                    else
                        scrollDown xs selectedItem isRunning

scrollUp : Menu -> MenuItem -> Bool -> MenuItem
scrollUp menu selectedItem isRunning =
    scrollDown (List.reverse menu) selectedItem isRunning

isSameItem : MenuItem -> MenuItem -> Bool
isSameItem a b = 
    case (a,b) of
        (Difficulty _, Difficulty _) -> True
        _ -> a == b
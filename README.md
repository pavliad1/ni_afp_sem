# NI_AFP_SEM

## Tetris

Cílem semestrální práce by bylo vytvoření hry tetris ve webovém prohlížeči pomocí jazyka Elm.

### Hlavní body
1. Implementace hlavního herního okna a "enginu"
    - Klasické herní objekty (L,Z,I,J,...)
    - Ovládání (Posuv doleva/doprava/dolů/fast posun dolů)
    - Počítání skóre
2. ~~Implementace predikčního okna (fronta dalších vygenerovaných objektů)~~
3. Implementace menu
    - Start nové hry/Restart
    - ~~High score~~
    - Možnost nastavení obtížnosti (rychlosti sestupu)
        - Statická obtížnost (neměnná na základě skóre)
        - Dynamická obtížnost (na základě skóre se mění až do určité maximální hodnoty)
        
### Zbuildění

elm make src/Main.elm

index.html je již zbuilděná aplikace

### Popis

#### Ovládání

1. Menu
    - W a S se mění položky menu
    - V případě vybrání položky Difficulty ji lze měnit pomocí A a D
        - Obtížnosti jsou buď statické 1-10 nebo dynamická, která na základě skóre zrychluje hru
    - Enter pak potvrzuje položku Start/Restart/Continue
2. Hra
    - A pohyb vlevo
    - D pohyb vpravo
    - W rotace objektu
    - S pohyb dolu
    - Space rychlý sestup
    - P pauza (skok do menu)